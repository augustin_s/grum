import numpy as np

from .descs import PlotDescription, ImageDescription


X = np.arange(100) / 10

exampledata_raw = {
    "sine":   [X, np.sin(X)],
    "cosine": [X, np.cos(X)],
    "exp":    [X, np.exp(X)],
    "log":    [X, np.log(X+1)]
}


for i in range(20):
    n = f"plot{i}"
    Y = np.sin(X) + 0.2 * i
    exampledata_raw[n] = [X, Y]


exampledata = {}
for name, (xs, ys) in exampledata_raw.items():
    exampledata[name] = PlotDescription(
        name,
#        title=name,
        xlabel="x",
        ylabel="y",
        xs=xs,
        ys=ys
    )


name = "image"
xdim = ydim = 100
size = xdim * ydim
shape = (xdim, ydim)

img = np.arange(size).reshape(shape) / size
img += np.random.random(shape) / 10

exampledata[name] = ImageDescription(
    name,
    image=img,
    xlabel="x",
    ylabel="y"
)



