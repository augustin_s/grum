from itertools import cycle
from PyQt5.QtGui import QPalette, QColor
import pyqtgraph as pg
from . import colors as C


def QGrey(i):
    return QColor(i, i, i)


HIGHLIGHT = QColor(C.VIOLET)
URL       = QColor(C.BLUE)

GREY0 = QGrey(25) # dark
GREY1 = QGrey(35)
GREY2 = QGrey(50)
GREY3 = QGrey(125)
GREY4 = QGrey(150)
GREY5 = QGrey(200) # light

WHITE = QGrey(255)


MDI_BKG = GREY4

DOT_PEN = GREY5
DOT_BRUSH = HIGHLIGHT
DOT_SIZE = 8

LINE_COLORS = (
    C.BLUE,
    C.BASE_GREEN,
    C.RED,
    C.YELLOW,
    C.VIOLET,
    C.ORANGE,
    C.GREEN2,
    C.COOL3,
    C.BROWN1,
    C.BASE_ORANGE,
)



def apply(app):
    apply_qapp(app)
    apply_pg()


def apply_qapp(app):
    app.setStyle("Fusion")
    pal = make_palette()
    app.setPalette(pal)


def make_palette():
    pal = QPalette()

    sc = pal.setColor
    qp = QPalette

    sc(qp.ButtonText,      WHITE)
    sc(qp.HighlightedText, WHITE)
    sc(qp.Text,            WHITE)
    sc(qp.ToolTipText,     WHITE)
    sc(qp.WindowText,      WHITE)

    sc(qp.Base,          GREY1)
    sc(qp.AlternateBase, GREY2)
    sc(qp.Window,        GREY2)
    sc(qp.Button,        GREY2)
    sc(qp.ToolTipBase,   GREY0)

    sc(qp.BrightText,  HIGHLIGHT)
    sc(qp.Highlight,   HIGHLIGHT)
    sc(qp.Link,        URL)
    sc(qp.LinkVisited, HIGHLIGHT)

    sc(qp.Active, qp.Button, GREY2)

    sc(qp.Disabled, qp.ButtonText, GREY3)
    sc(qp.Disabled, qp.WindowText, GREY3)
    sc(qp.Disabled, qp.Text,       GREY3)
    sc(qp.Disabled, qp.Light,      GREY2)

    return pal


def apply_pg():
    pg.setConfigOption("foreground", WHITE)
    pg.setConfigOption("background", GREY0)


def pg_plot_style(line_color=DOT_PEN, marker_stroke_color=DOT_PEN, marker_fill_color=DOT_BRUSH):
    pen = pg.mkPen(
        line_color,
        width=3
    )

    style = dict(
        pen=pen,
        symbol="o",
        symbolPen=marker_stroke_color,
        symbolBrush=marker_fill_color,
        symbolSize=DOT_SIZE
    )

    return style


def pg_plot_style_cycler(colors=LINE_COLORS):
    line_styles = (pg_plot_style(line_color=c, marker_fill_color=c) for c in colors)
    return cycle(line_styles)


def pg_legend_style():
    shade = QColor(0, 0, 0, 200)
    style = dict(
        brush = shade,
        pen = WHITE
    )
    return style



