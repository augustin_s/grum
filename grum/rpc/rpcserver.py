import xmlrpc.server as xrs
from inspect import getdoc, signature


class RPCServer(xrs.DocXMLRPCServer):

    def __init__(self, host, port, *args, doc_title_suffix="", **kwargs):
        addr = (host, port)
        kwargs.setdefault("allow_none", True)
        super().__init__(addr, *args, **kwargs)

        if doc_title_suffix:
            self.server_name  = doc_title_suffix + " " + self.server_name
            self.server_title = doc_title_suffix + " " + self.server_title

        self.register_function(self.ping, name="utils.ping")
        self.register_function(self.help, name="utils.help")
        self.register_function(self.info, name="utils.info")


    def ping(self):
        """
        Returns "pong". May be used for testing the connection.
        """
        return "pong"


    def help(self):
        """
        Returns an overview of exposed functions incl. signatures and docstrings.
        """
        info = self.info()
        lines = []
        for name, item in info.items():
            sig = item["signature"]
            head = f"{name}{sig}:"
            underline = "-" * len(head)
            doc = item["docstring"]
            doc = str(doc) # take care of None
            lines += [head, underline, doc, ""]
        return "\n".join(lines)


    def info(self):
        """
        Returns a dict mapping names of exposed functions to dicts holding their signature and docstring.
        """
        res = {}
        for name, func in self.funcs.items():
            doc = getdoc(func)
            sig = str(signature(func))
            res[name] = dict(signature=sig, docstring=doc)
        return res



