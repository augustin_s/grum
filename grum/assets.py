import os.path
from PyQt5.QtGui import QIcon, QPixmap


def get_asset(name):
    dname = os.path.dirname(__file__)
    return os.path.join(dname, "assets", name)


# These are functions in order not to trigger:
# Must construct a QGuiApplication before a QPixmap

logo = lambda: QPixmap(get_asset("logo.png"))
icon = lambda: QIcon(get_asset("icon.png"))
char = lambda: QIcon(get_asset("char.png"))



