from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtWidgets import QMenu
from PyQt5.QtGui import QCursor

from .menubase import MenuBase


class RClickMenu(MenuBase):

    def __init__(self, obj):
        super().__init__()
        self.qmenu = QMenu()
        obj.setContextMenuPolicy(Qt.CustomContextMenu)
        obj.customContextMenuRequested[QPoint].connect(self.on_show)

    def on_show(self):
        pos = QCursor.pos()
        self.qmenu.exec(pos)



