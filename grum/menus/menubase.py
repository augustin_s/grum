from PyQt5.QtGui import QKeySequence, QIntValidator
from PyQt5.QtWidgets import QAction, QActionGroup, QWidgetAction, QLineEdit


class MenuBase:
    """
    sub-classes need to create self.qmenu
    QActions need parent=self.qmenu to be set
    """

    def __init__(self):
        self.checkboxes = {}

    def addAction(self, name, triggered, shortcut=None):
        action = QAction(name, triggered=triggered, parent=self.qmenu)
        if shortcut:
            action.setShortcut(QKeySequence(shortcut))
        self.qmenu.addAction(action)
        return action

    def addCheckbox(self, name, state=False, triggered=None):
        # QAction does not accept triggered=None
        kwargs = dict(checkable=True, parent=self.qmenu)
        if triggered:
            kwargs.update(triggered=triggered)
        action = QAction(name, **kwargs)
        action.setChecked(state)
        self.qmenu.addAction(action)
        # remove shorthand & from name for easier retrieval
        name = name.replace("&", "")
        self.checkboxes[name] = action
        return action


    def addEntrybox(self, name, placeholder=None, state=False, triggered=None):
        cb = self.addCheckbox(name, state=state)

        edit = QLineEdit(self.qmenu)
        edit.setValidator(QIntValidator()) #TODO: make optional
        edit.setEnabled(state)
        if placeholder:
            edit.setPlaceholderText(placeholder)
        edit.setContentsMargins(8, 5, 8, 5) # mimic margins of other actions TODO: this probably depends on the theme

        @cb.toggled.connect
        def propagate_state_and_keep_menu_open_and_focus_edit(checked):
            edit.setEnabled(checked)
            if checked:
                self.qmenu.show()
                self.qmenu.setActiveAction(cb)
                edit.setFocus()

        @edit.returnPressed.connect
        def close_menu():
            self.qmenu.close()

        if triggered:
            @cb.triggered.connect
            def confirm_disabled(checked):
                if not checked:
                    triggered(None)

        if triggered:
            @edit.returnPressed.connect
            def confirm_value():
                value = edit.text()
                value = int(value)
                triggered(value)

        action = QWidgetAction(self.qmenu)
        action.setDefaultWidget(edit)
        self.qmenu.addAction(action)
        return action


    def addSeparator(self):
        self.qmenu.addSeparator()

    def addGroup(self):
        group = Group(self)
        return group



class Group:

    def __init__(self, menu):
        self.menu = menu
        self.qactiongroup = QActionGroup(menu.qmenu)


    def __getattr__(self, name):
        attr = getattr(self.menu, name)

        if not callable(attr):
            return attr

        # replace attr by a wrapper that automatically adds returned actions to the group
        def wrapper(*args, **kwargs):
            res = attr(*args, **kwargs)
            if isinstance(res, QAction):
                self.qactiongroup.addAction(res)
            return res

        return wrapper



