import fnmatch
import re

from PyQt5.QtWidgets import QWidget, QVBoxLayout

from .dictlistwidget import DictListWidget
from .searchbox import SearchBox


class DictList(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__()

        search = SearchBox()
        self.lst = lst = DictListWidget(*args, **kwargs)

        lay = QVBoxLayout()
        lay.setContentsMargins(0, 0, 0, 0)
        lay.setSpacing(0)
        lay.addWidget(lst)
        lay.addWidget(search)
        self.setLayout(lay)

        # QWidget has an update method already
        self.update = lst.update

        search.textChanged.connect(self.hide_not_matching)


    # behave like encapsulated DictListWidget
    def __getattr__(self, name):
        return getattr(self.lst, name)


    def hide_not_matching(self, pattern):
        g = Globber(pattern)
        for name, itm in self.lst.items.items():
            state = g.match(name)
            itm.setHidden(not state)



class Globber:

    def __init__(self, pattern):
        pattern = pattern.casefold()
        pattern = "*" + pattern + "*"
        regex = fnmatch.translate(pattern)
        self.pattern = re.compile(regex)

    def match(self, string):
        string = string.casefold()
        matches = self.pattern.match(string)
        state = bool(matches)
        return state



