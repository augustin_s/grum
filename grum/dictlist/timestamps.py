import functools
from datetime import datetime


DEFAULT_NAMES = (
    "creation",
    "access",
    "modification"
)


class Timestamps:

    def __init__(self, names=DEFAULT_NAMES):
        dt = datetime.now() # make sure all times are identical at the start
        self.times = {n: Time(dt) for n in names}
        self.__dict__.update(self.times)

    def max(self):
        return max(self.times.values())

    def min(self):
        return min(self.times.values())

    def __repr__(self):
        return repr(self.times)



@functools.total_ordering
class Time:

    def __init__(self, dt=None):
        self.dt = dt or datetime.now()

    def update(self):
        self.dt = datetime.now()

    def __repr__(self):
        return repr(self.dt)

    def __eq__(self, other):
        return (self.dt == other.dt)

    def __lt__(self, other):
        return (self.dt < other.dt)



