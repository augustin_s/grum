from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLineEdit, QPushButton, QHBoxLayout, QStyle

from ..shortcut import shortcut


class SearchBox(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__()

        self.txt = txt = QLineEdit(*args, **kwargs)
        txt.setPlaceholderText("Search...")

        btn = SquareButton()
        btn.setFlat(True)

        pmap = QStyle.SP_DialogCloseButton
        icon = self.style().standardIcon(pmap)
        btn.setIcon(icon)

        lay = QHBoxLayout()
        lay.setContentsMargins(2, 2, 0, 2)
        lay.setSpacing(0)
        lay.addWidget(txt)
        lay.addWidget(btn)
        self.setLayout(lay)

        btn.clicked.connect(self.txt.clear)

        #TODO: where should this go? probably top level MainWindow...
        shortcut(self, "Ctrl+F", txt.setFocus)


    # behave like encapsulated QLineEdit
    def __getattr__(self, name):
        return getattr(self.txt, name)


    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Escape:
            self.txt.clear()
        super().keyPressEvent(event)



class SquareButton(QPushButton):

    def resizeEvent(self, _event):
        height = self.height()
        self.setMinimumWidth(height)



