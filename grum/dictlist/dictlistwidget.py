from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidget

from .dictlistitem import DictListItem
from ..menus import RClickMenu
from ..shortcut import shortcut


class DictListWidget(QListWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setDragDropMode(QListWidget.InternalMove)
        self.setSelectionMode(QListWidget.ExtendedSelection)
        self.items = {}
        self._add_menu()

        shortcut(self, "Del", self.delete_selected, context=Qt.WidgetShortcut)

        self.nkeep = None
        self.model().rowsInserted.connect(self.on_evict)


    def _add_menu(self):
        self.menu = menu = RClickMenu(self)
        menu.addAction("Delete selected", self.delete_selected)
        menu.addAction("Delete all",      self.delete_all)

    def delete_selected(self):
        selected = self.selectedItems()
        for i in selected:
            self.items.pop(i.key)
            self.deleteItem(i)

    def deleteItem(self, itm):
        index = self.indexFromItem(itm)
        row = index.row()
        self.takeItem(row)

    def delete_all(self):
        self.items.clear()
        super().clear()


    def get(self, key):
        return self.items[key]

    def update(self, data):
        for k, v in data.items():
            self.set(k, v)

    def set(self, key, value):
        if key not in self.items:
            self._make_new_item(key, value)
        else:
            self.items[key].value = value

    def _make_new_item(self, key, value):
        itm = DictListItem(key, value)
        self.items[key] = itm
        self.prependItem(itm)

    def prependItem(self, itm):
        self.insertItem(0, itm)


    def selectedItems(self):
        selected = super().selectedItems()
        selected = [s for s in selected if not s.isHidden()]
        return selected


    def set_alarm_for_selected(self, state):
        selected = self.selectedItems()
        for i in selected:
            i.set_alarm(state)


    def set_sort_key(self, sk, reverse=True):
        DictListItem.set_sort_key(sk)

        if sk is None:
            self.setSortingEnabled(False)
        else:
            self.setSortingEnabled(True)
            order = Qt.DescendingOrder if reverse else Qt.AscendingOrder
            self.sortItems(order)


    def enable_sort_by_insertion_order(self):
        # map order in dict to indices
        mapping = enumerate(self.items.values())
        mapping = {item.text(): index for index, item in mapping}

        def unsort(x):
            return mapping[x.text()]

        self.set_sort_key(unsort)
        self.disable_sorting()


    def enable_sort_by_text(self):
        self.set_sort_key(lambda x: x.text(), reverse=False)


    def enable_sort_by_timestamp(self):
        # fall back to name for identical timestamps
        self.set_sort_key(lambda x: (x.timestamps.max(), x.text()))


    def disable_sorting(self):
        self.set_sort_key(None)


    def set_nkeep(self, n):
        self.nkeep = n
        self.on_evict()


    def on_evict(self):
        if self.nkeep:
            self.evict(self.nkeep)


    def evict(self, nkeep):
        items = self.items.values()

        # map order in dict to indices
        mapping = enumerate(items)
        mapping = {item.text(): index for index, item in mapping}

        def sk(x):
            # fall back to insertion order for identical timestamps
            return (x.timestamps.max(), mapping[x.text()])

        items = sorted(items, key=sk, reverse=True)
        items_to_evict = items[nkeep:]

        for i in items_to_evict:
            self.items.pop(i.key)
            self.deleteItem(i)



