from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtGui import QIcon, QPixmap, QPainter

from ..theme import DOT_PEN, DOT_BRUSH, DOT_SIZE
from .timestamps import Timestamps


class DictListItem(QListWidgetItem):

    def __init__(self, key, value, *args, **kwargs):
        super().__init__(key, *args, **kwargs)
        self.key = key
        self.value = value
        self.timestamps = Timestamps()
        self.set_alarm(False)


    _sort_key = None

    @classmethod # need to update the class so that all instances are always consistent
    def set_sort_key(cls, sk):
        cls._sort_key = staticmethod(sk) # need to attach as staticmethod, otherwise self is inserted as first argument

    def __lt__(self, other):
        assert self._sort_key == other._sort_key
        sk = self._sort_key
        if sk:
            return sk(self) < sk(other)
        else:
            return super().__lt__(other)


    def set_alarm(self, state):
        self.set_bold(state)
        self.set_dot(state)


    def set_bold(self, state):
        fnt = self.font()
        fnt.setBold(state)
        self.setFont(fnt)


    def set_dot(self, state):
        pixmap = QPixmap(DOT_SIZE, DOT_SIZE)
        pixmap.fill(Qt.transparent)

        if state:
            draw_dot(pixmap, DOT_PEN, DOT_BRUSH)

        icon = QIcon(pixmap)
        self.setIcon(icon)



def draw_dot(pixmap, pen, brush):
    painter = QPainter()
    painter.begin(pixmap)
    painter.setRenderHint(QPainter.Antialiasing)
    painter.setPen(pen)
    painter.setBrush(brush)
    rect = pixmap.rect()
    painter.drawEllipse(rect)
    painter.end()



