from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QShortcut


def shortcut(parent, key_sequence, triggered, **kwargs):
    key_sequence = QKeySequence(key_sequence)
    sc = QShortcut(key_sequence, parent, **kwargs)
    sc.activated.connect(triggered)
    return sc



