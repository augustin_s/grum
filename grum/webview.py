from PyQt5.QtCore import QUrl

try:
    from PyQt5.QtWebKitWidgets import QWebView
except ImportError:
    from PyQt5.QtWebEngineWidgets import QWebEngineView as QWebView


class WebView(QWebView):

    def __init__(self, url, title=None):
        super().__init__()
        self.url = QUrl(url)
        if title:
            self.setWindowTitle(title)

    def show(self):
        self.load(self.url)
        super().show()



