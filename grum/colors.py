
BASE_ORANGE = "#f8a600"
BASE_GREEN  = "#afca0b"

#GREY1 = "#575756"
#GREY2 = "#706f6f"
#GREY3 = "#9d9d9d"
#GREY4 = "#bdbdbd"
#GREY5 = "#e3e3e3"

YELLOW = "#ffcc00"
ORANGE = "#ea5b0c"
RED    = "#c00d0d"
BROWN1 = "#855642"
BROWN2 = "#8c7425"
GREEN1 = "#7d932e"
GREEN2 = "#107527"
VIOLET = "#792953"
BLUE   = "#003e6e"

COOL1 = "#00959e"
COOL2 = "#0097be"
COOL3 = "#0074a6"


