from .desc import Description


class PlotDescription(Description):

    def __init__(self, name, title=None, xlabel=None, ylabel=None, xs=None, ys=None):
        self.name = name
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.xs = [] if xs is None else list(xs)
        self.ys = [] if ys is None else list(ys)


    @property
    def data(self):
        return (self.xs, self.ys)

    @data.setter
    def data(self, value):
        self.xs, self.ys = value


    def append(self, xy):
        x, y = xy
        self.xs.append(x)
        self.ys.append(y)

    def extend(self, data):
        xs, ys = data
        self.xs.extend(xs)
        self.ys.extend(ys)


    def make_plot(self, plotwidget, style):
        res = plotwidget.plot(self.xs, self.ys, name=self.name, **style)

        if self.title:
            plotwidget.setTitle(self.title)

        if self.xlabel:
            plotwidget.setLabel("bottom", self.xlabel)

        if self.ylabel:
            plotwidget.setLabel("left", self.ylabel)

        return res



