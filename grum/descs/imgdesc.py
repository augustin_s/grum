import numpy as np
import pyqtgraph as pg
from .desc import Description


class ImageDescription(Description):

    def __init__(self, name, title=None, xlabel=None, ylabel=None, image=None, levels=None, cmap="viridis"):
        self.name = name
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.image = image
        self.levels = levels #TODO: might be better to use vmin and vmax
        self.cmap = cmap

    @property
    def data(self):
        return np.asarray(self.image)

    @data.setter
    def data(self, value):
        self.image = value


    def append(self, xy):
        print("ignored image append")

    def extend(self, data):
        print("ignored image extend")


    def make_plot(self, plotwidget, style):
        res = plotwidget.setImage(self.data, levels=self.levels)

        if self.title:
            plotwidget.setTitle(self.title)

        if self.xlabel:
            plotwidget.getView().setLabel("bottom", self.xlabel)

        if self.ylabel:
            plotwidget.getView().setLabel("left", self.ylabel)

        if self.cmap:
            cm = pg.colormap.get(self.cmap)
            plotwidget.setColorMap(cm)

        return res



