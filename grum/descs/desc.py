
class Description:

    def to_dict(self):
        res = {k: v for k, v in self.__dict__.items() if not k.startswith("_") and k != "name" and v is not None}
        tn = self.get_type()
        res.setdefault("type", tn)
        return res


    @classmethod
    def get_type(cls):
        tn = cls.__name__
        suffix = "Description"
        if not tn.endswith(suffix):
            raise ValueError(f'"{tn}" does not end with "{suffix}"')
        tn = tn[:-len(suffix)]
        tn = tn.casefold()
        tn = tn or None
        return tn



