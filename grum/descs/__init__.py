
from .desc import Description
from .imgdesc import ImageDescription
from .plotdesc import PlotDescription


DESC_TYPES = {
    ImageDescription.get_type(): ImageDescription,
    PlotDescription.get_type():  PlotDescription
}



