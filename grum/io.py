import h5py


def write_dict(fn, d):
    d = flatten_dict(d)
    with h5py.File(fn, "w") as f:
        for k, v in d.items():
            try:
                f[k] = v
            except Exception:
                print("could not store:", k, "<<", v)


def flatten_dict(d, parent="", sep="/"):
    res = {}
    for key, value in d.items():
        abs_key = parent + sep + key
        if isinstance(value, dict):
            deeper = flatten_dict(value, parent=abs_key, sep=sep)
            res.update(deeper)
        else:
            res[abs_key] = value
    return res


def read_dict(fn):
    res = {}

    def visit(name, node):
        if isinstance(node, h5py.Dataset):
            data = node[()]
            if isinstance(data, bytes):
                data = data.decode("utf-8")
            res[name] = data

    with h5py.File(fn, "r") as f:
        f.visititems(visit)

    return unflatten_dict(res)


def unflatten_dict(d, sep="/"):
    res = {}
    for k, v in d.items():
        current = res
        levels = k.split(sep)
        for l in levels[:-1]:
            if l not in current:
                current[l] = {}
            current = current[l]
        current[levels[-1]] = v
    return res



