import argparse
import sys

from PyQt5.QtWidgets import QApplication

from . import ctrl_c, theme
from .mainwin import MainWindow
from .mdi import MDIWindowMode


def main():
    app = QApplication(sys.argv)
    ctrl_c.setup(app)

    clargs = handle_clargs()
    if not clargs.pop("no_theme"):
        theme.apply(app)

    mw = MainWindow(**clargs)
    mw.show()
    sys.exit(app.exec())


def handle_clargs():
    DESC = "grum - GUI for Remote Unified Monitoring"
    parser = argparse.ArgumentParser(description=DESC, formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-H", "--host", default="localhost", help="RPC server host name")
    parser.add_argument("-P", "--port", default=8000, type=int, help="RPC server port number")
    parser.add_argument("-o", "--offline", action="store_true", help="offline mode (do not run RPC server)")

    parser.add_argument("-w", "--window-mode", default="multi", choices=MDIWindowMode.values(), type=unambiguous_window_mode, help="set the initial window mode")

    parser.add_argument("-e", "--examples", dest="add_examples", action="store_true", help="add example data")
    parser.add_argument("--no-theme", action="store_true", help="disable theming")

    return parser.parse_args().__dict__


def unambiguous_window_mode(arg):
    cfarg = arg.casefold()
    values = MDIWindowMode.values()
    matches = [i for i in values if i.casefold().startswith(cfarg)]
    if len(matches) == 1:
        return matches[0]
    return arg



