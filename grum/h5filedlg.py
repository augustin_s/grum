from PyQt5.QtWidgets import QFileDialog


DEFAULT_SUFFIX = "h5"

FILETYPE_FILTERS = [
    "HDF5 files (*.h5)",
    "All files (*)"
]

FILETYPE_FILTERS = ";;".join(FILETYPE_FILTERS)


def save_h5_file_dialog(parent):
    fn, _chosen_filter = getSaveFileName(parent, filter=FILETYPE_FILTERS, defaultSuffix=DEFAULT_SUFFIX)
    return fn

def open_h5_files_dialog(parent):
    fns, _chosen_filter = QFileDialog.getOpenFileNames(parent, filter=FILETYPE_FILTERS)
    return fns


def getSaveFileName(
    parent=None, caption="", dir="", filter="",
    selectedFilter=None, defaultSuffix=None, options=None
):
    """
    Re-implementation of QFileDialog.getSaveFileName that allows to set defaultSuffix
    """
    dlg = QFileDialog(parent=parent, caption=caption, directory=dir, filter=filter)

    if selectedFilter: dlg.selectNameFilter(selectedFilter)
    if defaultSuffix:  dlg.setDefaultSuffix(defaultSuffix)
    if options:        dlg.setOptions(options)

    dlg.setAcceptMode(QFileDialog.AcceptSave)
    dlg.setFileMode(QFileDialog.AnyFile)

    res = dlg.exec()
    if res == QFileDialog.Rejected:
        return "", ""

    fns = dlg.selectedFiles()
    assert len(fns) == 1
    fn = fns[0]

    selectedFilter = dlg.selectedNameFilter()

    return fn, selectedFilter



