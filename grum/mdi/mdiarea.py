import enum
from PyQt5.QtWidgets import QMdiArea
from PyQt5.QtGui import QPainter

from .. import assets
from ..theme import MDI_BKG
from ..menus import BarMenu


class MDIWindowMode(str, enum.Enum):
    MULTI  = "multi"
    SINGLE = "single"
    TABS   = "tabs"

    @classmethod
    def values(cls):
        return tuple(i.value for i in cls)



class MDIArea(QMdiArea):

    def __init__(self, bar, *args, window_mode=MDIWindowMode.MULTI, **kwargs):
        super().__init__(*args, **kwargs)
        self.logo = assets.logo()
        self.setTabsClosable(True)
        self.setTabsMovable(True)
        self._add_menu(bar)
        self.set_window_mode(window_mode)


    def _add_menu(self, bar):
        self.menu = menu = BarMenu(bar, "&Windows")
        menu.addAction("Cascade", self.on_cascade, shortcut="Ctrl+C")
        menu.addAction("Tile",    self.on_tile,    shortcut="Ctrl+T")
        menu.addSeparator()
        group = menu.addGroup()
        group.addCheckbox("&Multiple windows", triggered=self.enable_multiple_windows_mode, state=True)
        group.addCheckbox("&Single window",    triggered=self.enable_single_window_mode)
        group.addCheckbox("&Tabbed",           triggered=self.enable_tabbed_mode)
        menu.addSeparator()
        menu.addAction("Close active",   self.closeActiveSubWindows)
        menu.addAction("Close inactive", self.closeInactiveSubWindows)
        menu.addAction("Close all",      self.closeAllSubWindows)


    def set_window_mode(self, mode: MDIWindowMode) -> None:
        mode_enablers = {
            MDIWindowMode.SINGLE: self.enable_single_window_mode,
            MDIWindowMode.MULTI:  self.enable_multiple_windows_mode,
            MDIWindowMode.TABS:   self.enable_tabbed_mode
        }
        enable_mode = mode_enablers[mode]
        enable_mode()


    def on_cascade(self):
        self.enable_multiple_windows_mode()
        self.cascadeSubWindows()

    def on_tile(self):
        self.enable_multiple_windows_mode()
        self.tileSubWindows()


    def enable_multiple_windows_mode(self):
        self.menu.checkboxes["Multiple windows"].setChecked(True)
        self.enable_subwindow_view()
        for sub in self.subWindowList():
            sub.restore()
            sub.frame_on()

    def enable_single_window_mode(self):
        self.menu.checkboxes["Single window"].setChecked(True)
        self.enable_subwindow_view()
        self.closeInactiveSubWindows()
        active = self.activeSubWindow()
        if active:
            active.maximize()
            active.frame_off()

    def enable_tabbed_mode(self):
        self.menu.checkboxes["Tabbed"].setChecked(True)
        self.enable_tabbed_view()


    def enable_subwindow_view(self):
        self.setViewMode(QMdiArea.SubWindowView)

    def enable_tabbed_view(self):
        self.setViewMode(QMdiArea.TabbedView)


    def add(self, sub):
        single = self.menu.checkboxes["Single window"].isChecked()
        if single:
            self.add_single(sub)
        else:
            self.add_multiple(sub)

    def add_multiple(self, sub):
        self.addSubWindow(sub)
        sub.show()

    def add_single(self, sub):
        self.closeAllSubWindows()
        self.addSubWindow(sub)
        sub.maximize()
        sub.frame_off()


    def paintEvent(self, _event):
        self._draw_watermark()


    def _draw_watermark(self):
        painter = QPainter()
        vport = self.viewport()
        painter.begin(vport)

        rect = self.rect()
        painter.fillRect(rect, MDI_BKG)

        margin = 20
        logo = self.logo
        logo_width  = logo.width()
        logo_height = logo.height()
        painter.drawPixmap(
            self.width()  - logo_width  - margin,
            self.height() - logo_height - margin,
            logo_width,
            logo_height,
            logo
        )

        painter.end()


    def findSubWindow(self, name):
        for sub in self.subWindowList():
            if sub.windowTitle() == name:
                return sub


    def closeActiveSubWindows(self):
        active = self.activeSubWindow()
        if active:
            active.close()

    def closeInactiveSubWindows(self):
        active = self.activeSubWindow()
        for sub in self.subWindowList():
            if sub != active:
                sub.close()



