import pyqtgraph as pg
from .mdisubwin import MDISubWindow
from ..theme import pg_plot_style


class MDISubImage(MDISubWindow):

    def __init__(self, name, desc, *args, **kwargs):
        super().__init__(name, *args, **kwargs)
        self.pw = pw = pg.ImageView(view=pg.PlotItem()) # for axis ticks and labels, view needs to be a PlotItem
        self.setWidget(pw)

        # connect to plot mouse-over event
        pw.scene.sigMouseMoved.connect(self.on_hover)

        style = pg_plot_style()

        plot = desc.make_plot(self.pw, style)
        self.plots = {name: plot}

        self.image = desc.data


    def on_hover(self, event):
        coord = self.pw.imageItem.mapFromScene(event)
        x = coord.x()
        y = coord.y()
        x = int(x)
        y = int(y)
        try:
            z = self.image[x, y]
        except IndexError:
            return
        z = round(z, 3)
        self.setToolTip(f"x = {x}\ny = {y}\nz = {z}")



