import pyqtgraph as pg
from .mdisubwin import MDISubWindow
from ..theme import pg_plot_style, pg_plot_style_cycler, pg_legend_style


class MDISubPlotBase(MDISubWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pw = pw = pg.PlotWidget()
        self.setWidget(pw)

        # connect to plot mouse-over event
        pw.scene().sigMouseMoved.connect(self.on_hover)


    def on_hover(self, event):
        coord = self.pw.plotItem.vb.mapSceneToView(event)
        x = coord.x()
        y = coord.y()
        x = round(x, 3)
        y = round(y, 3)
        self.setToolTip(f"x = {x}\ny = {y}")



class MDISubPlot(MDISubPlotBase):

    def __init__(self, name, desc, *args, **kwargs):
        super().__init__(name, *args, **kwargs)

        style = pg_plot_style()

        plot = desc.make_plot(self.pw, style)
        self.plots = {name: plot}



class MDISubMultiPlot(MDISubPlotBase):

    def __init__(self, name, descs, *args, **kwargs):
        super().__init__(name, *args, **kwargs)

        ls = pg_legend_style()
        psc = pg_plot_style_cycler()

        pw = self.pw
        pw.addLegend(**ls)
        self.plots = {name: desc.make_plot(pw, style) for (name, desc), style in zip(descs.items(), psc)}



