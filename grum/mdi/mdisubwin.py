from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMdiSubWindow

from .. import assets


SUB_WIN_WIDTH  = 640
SUB_WIN_HEIGHT = 480


class MDISubWindow(QMdiSubWindow):

    def __init__(self, title, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWindowTitle(title)
        self.setWindowIcon(assets.char())
        self.resize(SUB_WIN_WIDTH, SUB_WIN_HEIGHT)

        # without this, the SubWindow is not removed from the subWindowList
        self.setAttribute(Qt.WA_DeleteOnClose)

        self._previous_state = None


    def maximize(self):
        self._previous_state = state = self.windowState()
        self.setWindowState(state | Qt.WindowMaximized)

    def restore(self):
        if self._previous_state:
            self.setWindowState(self._previous_state)


    def frame_on(self):
        self.hide()
        self.setWindowFlags(Qt.SubWindow)
        self.show()

    def frame_off(self):
        self.hide()
        self.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        self.show()



