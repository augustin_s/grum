from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QMainWindow, QSplitter

from . import assets
from .descs import DESC_TYPES, Description, ImageDescription, PlotDescription
from .dictlist import DictList
from .exampledata import exampledata
from .h5filedlg import open_h5_files_dialog, save_h5_file_dialog
from .io import write_dict, read_dict
from .mdi import MDIArea, MDISubMultiPlot, MDISubPlot, MDISubImage, MDIWindowMode
from .menus import BarMenu
from .rpc import RPCServerThread
from .shortcut import shortcut
from .webview import WebView


DESC_TYPE_TO_MDI_SUB_TYPE = {
    ImageDescription: MDISubImage,
    PlotDescription:  MDISubPlot
}


class MainWindow(QMainWindow):

    sig_make_new_subwin  = pyqtSignal(str, Description)

    def __init__(self, *args, title="grum", host="localhost", port=8000, offline=False, add_examples=False, window_mode=MDIWindowMode.MULTI, **kwargs):
        super().__init__(*args, **kwargs)

        if offline:
            title = f"{title} (offline)"

        self.setWindowTitle(title)
        self.setWindowIcon(assets.icon())

        url = f"http://{host}:{port}/"
        self.webdoc = WebView(url, title=title)

        self.lst = lst = DictList()
        lst.setAlternatingRowColors(True)
        lst.itemDoubleClicked.connect(self.on_dclick_list_item)

        if add_examples:
            lst.update(exampledata)

        lst_menu = lst.lst.menu
        lst_menu.addSeparator()
        lst_menu.addAction("Plot selected", self.on_plot_selected)
        lst_menu.addSeparator()
        lst_menu.addAction("Mark selected as seen", self.on_mark_selected_as_seen)
        lst_menu.addAction("Mark selected as not seen", self.on_mark_selected_as_not_seen)
        lst_menu.addSeparator()
        sort_group = lst_menu.addGroup()
        sort_group.addCheckbox("Sort by insertion order", triggered=self.on_sort_by_insertion_order, state=True)
        sort_group.addCheckbox("Sort by name", triggered=self.on_sort_by_name)
        sort_group.addCheckbox("Sort by timestamp", triggered=self.on_sort_by_timestamp)
        sort_group.addCheckbox("Sorting disabled", triggered=self.on_sorting_disabled)

        #TODO: clean up
        def on_item_about_to_be_moved():
            sort_group.checkboxes["Sorting disabled"].setChecked(True)
            self.on_sorting_disabled()

        self.lst.model().rowsAboutToBeMoved.connect(on_item_about_to_be_moved)

        shortcut(self, "Ctrl+P", self.on_plot_selected)

        bar = self.menuBar()

        self.menu_file = menu = BarMenu(bar, "&File")
        menu.addAction("&Open...", self.on_file_open, shortcut="Ctrl+O")
        menu.addAction("&Save as...", self.on_file_save, shortcut="Ctrl+S")
        menu.addSeparator()
        menu.addAction("&Quit", self.close, shortcut="Ctrl+Q")

        self.menu_settings = menu = BarMenu(bar, "&Settings")
        menu.addCheckbox("Open new plots", state=True)
        menu.addSeparator()
        menu.addEntrybox("Limit number of entries", placeholder="Maximum number of entries", triggered=lst.set_nkeep)

        self.mdi = mdi = MDIArea(bar, window_mode=window_mode)

        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(lst)
        splitter.addWidget(mdi)
        splitter.setSizes([100, 1000])

        self.setCentralWidget(splitter)

        if not offline:
            self.rst = rst = RPCServerThread(host, port, doc_title_suffix=title)
            rst.start()
            rst.server.register_function(self.new_image)
            rst.server.register_function(self.new_plot)
            rst.server.register_function(self.append_data)
            rst.server.register_function(self.extend_data)
            rst.server.register_function(self.set_data)

        self.sig_make_new_subwin.connect(self.on_make_new_subwin)


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_F1:
            self.webdoc.show()


    # Remote API calls

    def new_image(self, name, cfg):
        """
        Create a new image <name> using the configuration dict <cfg>.
        The configuration is forwarded to the constructor of ImageDescription.
        Allowed keys are: title, xlabel, ylabel, image, levels, cmap.
        """
        desc = self.add_new_desc_to_list(ImageDescription, name, cfg)
        if self.menu_settings.checkboxes["Open new plots"].isChecked():
            sub = self.mdi.findSubWindow(name)
            if sub:
                sub.pw.setImage(desc.data) #TODO lacks the list sync
            else:
                self.sig_make_new_subwin.emit(name, desc)

    def new_plot(self, name, cfg):
        """
        Create a new plot <name> using the configuration dict <cfg>.
        The configuration is forwarded to the constructor of PlotDescription.
        Allowed keys are: title, xlabel, ylabel, xs, ys.
        """
        desc = self.add_new_desc_to_list(PlotDescription, name, cfg)
        if self.menu_settings.checkboxes["Open new plots"].isChecked():
            if not self.mdi.findSubWindow(name):
                self.sig_make_new_subwin.emit(name, desc)

    def append_data(self, name, point):
        """
        Append a new data point <point> to the (existing) plot <name>.
        The point is forwarded to the append method of PlotDescription.
        """
        item = self.lst.get(name)
        desc = item.value
        desc.append(point)
        self.sync_item_and_plots(item)

    def extend_data(self, name, data):
        """
        Extend the current data of the (existing) plot <name>.by <data>
        The data is forwarded to the extend method of PlotDescription.
        """
        item = self.lst.get(name)
        desc = item.value
        desc.extend(data)
        self.sync_item_and_plots(item)

    def set_data(self, name, data):
        """
        Set <data> as the data of the (existing) plot <name>.
        The data is assigned to the data attribute of PlotDescription.
        """
        item = self.lst.get(name)
        desc = item.value
        desc.data = data
        self.sync_item_and_plots(item)


    # Signal callbacks

    def on_make_new_subwin(self, name, desc):
        DescType = type(desc)
        MDISubType = DESC_TYPE_TO_MDI_SUB_TYPE[DescType]
        self.make_subwin(MDISubType, name, desc)

    def on_dclick_list_item(self, item):
        self.plot_single_item(item)

    def on_plot_selected(self):
        selected = self.lst.selectedItems()
        if not selected:
            return
        if len(selected) == 1:
            item = selected[0]
            self.plot_single_item(item)
        else:
            self.plot_multiple_items(selected)


    def on_mark_selected_as_seen(self):
        self.lst.set_alarm_for_selected(False)

    def on_mark_selected_as_not_seen(self):
        self.lst.set_alarm_for_selected(True)


    def on_sort_by_insertion_order(self):
        self.lst.enable_sort_by_insertion_order()

    def on_sort_by_name(self):
        self.lst.enable_sort_by_text()

    def on_sort_by_timestamp(self):
        self.lst.enable_sort_by_timestamp()

    def on_sorting_disabled(self):
        self.lst.disable_sorting()


    def on_file_open(self):
        fns = open_h5_files_dialog(self)
        if not fns:
            return

        for fn in fns:
            data = read_dict(fn)
            for name, cfg in data.items():
                tn = cfg.pop("type")
                DescType = DESC_TYPES[tn]
                self.add_new_desc_to_list(DescType, name, cfg)


    def on_file_save(self):
        fn = save_h5_file_dialog(self)
        if not fn:
            return

        data = {}
        for name, item in self.lst.items.items():
            desc = item.value
            data[name] = desc.to_dict()

        write_dict(fn, data)


    # Plumbing

    def add_new_desc_to_list(self, DescType, name, cfg):
        desc = DescType(name, **cfg)
        self.lst.set(name, desc)
        return desc

    def sync_item_and_plots(self, item):
        name, desc = item.key, item.value
        alarm = True
        for sub in self.mdi.subWindowList():
            if name in sub.plots:
                plot = sub.plots[name]
                plot.setData(*desc.data)
                alarm = False
        item.timestamps.modification.update()
        item.set_alarm(alarm)

    def plot_single_item(self, item):
        item.timestamps.access.update()
        item.set_alarm(False)
        name, desc = item.key, item.value
        DescType = type(desc)
        MDISubType = DESC_TYPE_TO_MDI_SUB_TYPE[DescType]
        self.activate_or_make_subwin(MDISubType, name, desc)

    def plot_multiple_items(self, items):
        for i in items:
            i.timestamps.access.update()
            i.set_alarm(False)
        items = (i for i in items if isinstance(i.value, PlotDescription)) #TODO: for now, only overlay plots
        descs = {i.key: i.value for i in items}
        names = descs.keys()
        name = " | ".join(names)
        self.activate_or_make_subwin(MDISubMultiPlot, name, descs)


    #TODO: the following two could be methods of MDIArea?

    def activate_or_make_subwin(self, MDISubType, name, *args, **kwargs):
        sub = self.mdi.findSubWindow(name)
        if sub: #TODO check type? what to do for mismatches?
            self.mdi.setActiveSubWindow(sub)
        else:
            self.make_subwin(MDISubType, name, *args, **kwargs)

    def make_subwin(self, MDISubType, name, *args, **kwargs):
        sub = MDISubType(name, *args, **kwargs)
        self.mdi.add(sub)



