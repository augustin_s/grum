# grum

<img src="https://gitlab.psi.ch/augustin_s/grum/-/wikis/uploads/1a259a1d74e7b79e0230e7bbad3b1284/screenshot2.png" width="50%" />

## Overview

grum is a plotting GUI (using [pyqt5](https://www.riverbankcomputing.com/software/pyqt/)/[pyqtgraph](https://www.pyqtgraph.org/)) with an embedded RPC server (using [xmlrpc](https://docs.python.org/3/library/xmlrpc.html)).

In the GUI, a list/history can be used to open individual or overlay several plots.

Via the RPC server, new plots can be created and new data appended to existing plots.

## GUI

- Selecting items in the list allows the usual shortcuts (ctrl-/shift-click, `ctrl+a`, etc.).
- The selected items can be plotted using `ctrl+p`, with more options in the right-click menu.
- The list of plots is searchable via the bottom left search box or by pressing `ctrl+f`.

## API

### User functions

- `new_plot(name, cfg)`

Creates a new plot named `name` in the grum list. The configuration dict `cfg` is used as arguments for the constructor of [`PlotDescription`](https://gitlab.psi.ch/augustin_s/grum/-/blob/master/grum/descs/plotdesc.py#L4).

- `append_data(name, point)`

Append data point to the plot named `name`. The new `point` is forwarded to [`PlotDescription.append()`](https://gitlab.psi.ch/augustin_s/grum/-/blob/master/grum/descs/plotdesc.py#L18).

### Utility functions

- `utils.ping()`
- `utils.help()`
- `utils.info()`
