import sys
import pytest
from PyQt5.QtWidgets import QApplication
import pyqtgraph as pg

from grum import theme
from grum.mainwin import MainWindow
from grum.mdi.mdisubplot import MDISubPlot
from grum.descs import PlotDescription
from grum.theme import pg_plot_style


@pytest.mark.parametrize(
    "plot_name, plot_title, plot_xlabel, plot_ylabel, xs,ys",
    [
        ("plot_name", "plot_title", "plot_xlabel", "plot_ylabel", [1, 2], [3, 4]),
        ("plot_name", None, None, None, [], []),
    ],
)
def test_plot_description_init(
    plot_name, plot_title, plot_xlabel, plot_ylabel, xs, ys
):
    if plot_title:
        pd = PlotDescription(plot_name, plot_title, plot_xlabel, plot_ylabel, xs, ys)
    else:
        pd = PlotDescription(plot_name)

    assert pd.name == plot_name
    assert pd.title == plot_title
    assert pd.xlabel == plot_xlabel
    assert pd.ylabel == plot_ylabel
    assert pd.xs == xs
    assert pd.ys == ys


def test_data():
    pd = PlotDescription("name", xs=[1, 2], ys=[3, 4])

    assert pd.data == ([1, 2], [3, 4])


def test_append():
    pd = PlotDescription("name", xs=[1, 2], ys=[3, 4])
    pd.append([3, 5])

    assert pd.xs == [1, 2, 3]
    assert pd.ys == [3, 4, 5]


def test_make_plot():
    pd = PlotDescription(
        "plot_name", "plot_title", "plot_xlabel", "plot_ylabel", [1, 2], [3, 4]
    )
    app = QApplication(sys.argv)
    theme.apply(app)
    mw = MainWindow(add_examples=True)

    mdi_sub = MDISubPlot("mdi", pd)
    pw = pg.PlotWidget()
    style = pg_plot_style()

    ret = pd.make_plot(pw, style)

    assert isinstance(ret, pg.PlotDataItem)
    assert pw.getAxis("left").labelText == "plot_ylabel"
    assert pw.getAxis("bottom").labelText == "plot_xlabel"
    assert pw.getPlotItem().titleLabel.text == "plot_title"


def test_to_dict():
    pd = PlotDescription(
        "plot_name", "plot_title", "plot_xlabel", "plot_ylabel", [1, 2], [3, 4]
    )

    assert pd.to_dict() == {
        "title": "plot_title",
        "xlabel": "plot_xlabel",
        "xs": [1, 2],
        "ylabel": "plot_ylabel",
        "ys": [3, 4],
        "type": "plot"
    }



