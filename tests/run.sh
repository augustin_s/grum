#!/bin/bash

cd $(dirname $0)
cd ..

export QT_QPA_PLATFORM=offscreen

coverage run --source=./grum/ -m pytest ./tests/
echo
coverage report


