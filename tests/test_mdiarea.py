import sys
from unittest import mock

from grum.mainwin import MainWindow
from grum.mdi import MDIArea, MDISubMultiPlot, MDISubPlot
from grum.mdi.mdisubwin import MDISubWindow
from grum.theme import MDI_BKG

from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QApplication, QMdiArea


class TestMDIArea:

    def setup_method(self):
        print("setup")
        self.app = QApplication(sys.argv)
        self.mw = MainWindow(add_examples=True)
        self.mw.show()


    def teardown_method(self):
        print("teardown")
        self.mw.rst.wait_for_stop()
        self.app.quit()
        print("app quit called")
        del self.mw
        del self.app


    def test_on_cascade(self):
        mdi = self.mw.mdi

        mdi.enable_subwindow_view = mock.MagicMock()
        mdi.cascadeSubWindows = mock.MagicMock()
        mdi.menu.checkboxes["Multiple windows"].setChecked(False)
        assert mdi.menu.checkboxes["Multiple windows"].isChecked() == False

        mdi.on_cascade()

        assert mdi.menu.checkboxes["Multiple windows"].isChecked() == True
        mdi.enable_subwindow_view.assert_called_once()
        mdi.cascadeSubWindows.assert_called_once()


    def test_on_tile(self):
        mdi = self.mw.mdi

        mdi.enable_subwindow_view = mock.MagicMock()
        mdi.tileSubWindows = mock.MagicMock()
        mdi.menu.checkboxes["Multiple windows"].setChecked(False)
        assert mdi.menu.checkboxes["Multiple windows"].isChecked() == False

        mdi.on_tile()

        assert mdi.menu.checkboxes["Multiple windows"].isChecked() == True
        mdi.enable_subwindow_view.assert_called_once()
        mdi.tileSubWindows.assert_called_once()


    def test_enable_single_window_mode(self):
        mdi = self.mw.mdi
        mdi.enable_subwindow_view = mock.MagicMock()
        mdi.closeInactiveSubWindows = mock.MagicMock()
        active = mdi.activeSubWindow()
        assert  mdi.activeSubWindow() == None

        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mdi.addSubWindow(sub)
        mdi.setActiveSubWindow(MDISubWindow("sine"))

        # assert  mdi.activeSubWindow() != None #TODO: check this!

        mdi.enable_single_window_mode()

        mdi.enable_subwindow_view.assert_called_once()
        mdi.closeInactiveSubWindows.assert_called_once()

        # active = mdi.activeSubWindow()
        # if active:
            # active.showMaximized()
            # active.frame_off()


    def test_enable_subwindow_view(self):
        mdi = self.mw.mdi

        mdi.setViewMode = mock.MagicMock()
        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mdi.addSubWindow(sub)
        sub.frame_on = mock.MagicMock()

        mdi.enable_subwindow_view()

        mdi.setViewMode.assert_called_once_with(QMdiArea.SubWindowView)

#        sub.frame_on.assert_called_once() #TODO: check this


    def test_enable_tabbed_view(self):
        mdi = self.mw.mdi
        mdi.setViewMode = mock.MagicMock()

        mdi.enable_tabbed_view()
        mdi.setViewMode.assert_called_once_with(QMdiArea.TabbedView)


    def test_add(self):
        mdi = self.mw.mdi
        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mdi.add_multiple = mock.MagicMock()
        mdi.add_single = mock.MagicMock()
        assert mdi.menu.checkboxes["Single window"].isChecked() == False

        mdi.add(sub)
        mdi.add_multiple.assert_called_once_with(sub)

        mdi.menu.checkboxes['Single window'].setChecked(True)
        assert mdi.menu.checkboxes["Single window"].isChecked() == True

        mdi.add(sub)
        mdi.add_single.assert_called_once_with(sub)


    def test_add_multiple(self):
        mdi = self.mw.mdi
        mdi.addSubWindow = mock.MagicMock()
        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        sub.show = mock.MagicMock()

        mdi.add_multiple(sub)
        mdi.addSubWindow.assert_called_once()
        sub.show.assert_called_once()


    def test_add_single(self):
        mdi = self.mw.mdi
        mdi.addSubWindow = mock.MagicMock()
        mdi.closeAllSubWindows = mock.MagicMock()
        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        sub.maximize = mock.MagicMock()
        sub.frame_off = mock.MagicMock()

        mdi.add_single(sub)

        mdi.closeAllSubWindows.assert_called_once()
        mdi.addSubWindow.assert_called_once_with(sub)
        sub.maximize.assert_called_once()
        sub.frame_off.assert_called_once()


    def test_paintEvent(self):
        mdi = self.mw.mdi

        mdi._draw_watermark = mock.MagicMock()
        mdi.paintEvent('event')
        mdi._draw_watermark.assert_called_once()


    def test_findSubWindow(self):
        mdi = self.mw.mdi
        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mdi.add(sub)

        ret = mdi.findSubWindow('sine')

        assert ret == sub

        ret = mdi.findSubWindow('cosine')

        assert ret == None


    def test_closeInactiveSubWindows(self):
        mdi = self.mw.mdi

        sine_item = self.mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mdi.add(sub)
        cosine_item = self.mw.lst.lst.get("cosine")
        cosine_sub = MDISubPlot("cosine", cosine_item.value)
        mdi.add(cosine_sub)
        sub.close = mock.MagicMock()

        mdi.closeInactiveSubWindows()

        active = mdi.activeSubWindow()
        assert active == cosine_sub
        for sub in mdi.subWindowList():
            if sub != active:
                sub.close.assert_called_once()



