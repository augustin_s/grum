# import pytest
import sys
# from parameterized import parameterized
from unittest import mock

from PyQt5.QtTest import QSignalSpy
from PyQt5.QtWidgets import QApplication, QAction, QListWidget

from grum.cli import main
from grum.dictlist.dictlistitem import DictListItem
from grum.dictlist import DictList
from grum import ctrl_c, theme, assets
from grum.mainwin import MainWindow
from grum.mdi import MDIArea, MDISubMultiPlot, MDISubPlot
from grum.menus import BarMenu
from grum.menus.rclickmenu import RClickMenu
from grum.descs import Description, PlotDescription
from grum.rpc import RPCServerThread


class TestMainWin:

    def setup_method(self):
        print("setup")
        self.app = QApplication(sys.argv)
        theme.apply(self.app)
        # ctrl_c.setup(self.app)
        self.mw = MainWindow(add_examples=True)
        self.mw.show()


    def teardown_method(self):
        print("teardown")
        self.mw.rst.wait_for_stop()
        self.app.quit()
        print("app quit called")
        del self.mw
        del self.app


    def test_defaults(self):
        mw = self.mw

        assert mw.windowTitle() == "grum"
        # assert mw.windowIcon() == assets.icon()
        assert isinstance(mw.lst, DictList)

        for key in mw.lst.lst.items:
            assert isinstance(mw.lst.lst.get(key), DictListItem)
            assert isinstance(mw.lst.lst.get(key).value, Description)

        assert isinstance(mw.lst.menu, RClickMenu)
        assert isinstance(mw.menu_settings, BarMenu)

        assert isinstance(mw.menu_settings.checkboxes["Open new plots"], QAction)

        assert isinstance(mw.mdi, MDIArea)


    # @parameterized.expand([["Dotted line", "r"], ["Line", "r"], ["Dots", None]])
    def test_new_plot(self):
        mw = self.mw
        name = "plot_name"
        title = "plot_title"
        xlabel = "xlabel"
        ylabel = "ylabel"
        cfg = {"title": title, "xlabel": xlabel, "ylabel": ylabel}
        spy_sig_make_new_subwin = QSignalSpy(mw.sig_make_new_subwin)

        mw.new_plot(name, cfg=cfg)

        assert name in mw.lst.lst.items.keys()
        assert isinstance(mw.lst.lst.get(name).value, PlotDescription)
        assert mw.lst.lst.get(name).value.title == title
        assert mw.lst.lst.get(name).value.xlabel == xlabel
        assert mw.lst.lst.get(name).value.ylabel == ylabel

        assert mw.menu_settings.checkboxes["Open new plots"].isChecked()
        assert len(spy_sig_make_new_subwin) == 1  # assert called once
        assert spy_sig_make_new_subwin[0][0] == name  # assert called with name
        assert isinstance(spy_sig_make_new_subwin[0][1], PlotDescription)

        mw.menu_settings.checkboxes["Open new plots"].setChecked(False)
        assert mw.menu_settings.checkboxes["Open new plots"].isChecked() == False
        spy_sig_make_new_subwin = QSignalSpy(mw.sig_make_new_subwin)

        mw.new_plot("new_name", cfg)

        assert len(spy_sig_make_new_subwin) == 0  # assert not called

        mw.menu_settings.checkboxes["Open new plots"].setChecked(True)
        assert mw.menu_settings.checkboxes["Open new plots"].isChecked() == True
        spy_sig_make_new_subwin = QSignalSpy(mw.sig_make_new_subwin)
        new_name_item = mw.lst.lst.get("new_name")
        sub = MDISubPlot("new_name", new_name_item.value)
        mw.mdi.add(sub)

        mw.new_plot("new_name", cfg)

        assert len(spy_sig_make_new_subwin) == 0  # assert not called


    def test_append_data(self):
        mw = self.mw
        sine_item = mw.lst.lst.get("sine")
        sine_item.set_alarm = mock.MagicMock()
        sine_item.value.append = mock.MagicMock()

        point = (1, 2)

        mw.append_data("sine", point)
        sine_item.value.append.assert_called_once_with(point)
        sine_item.set_alarm.assert_called_once_with(True)

        sub = MDISubPlot("sine", sine_item.value)
        mw.mdi.add(sub)
        sub.plots["sine"].setData = mock.MagicMock()

        mw.append_data("sine", point)
        sub.plots["sine"].setData.assert_called_once_with(*sine_item.value.data)
        print(sine_item.set_alarm.call_args)
        assert sine_item.set_alarm.call_args[0][0] == False


    def test_on_make_new_subwin(self):
        mw = self.mw

        mw.make_subwin = mock.MagicMock()

        name = "test"
        cfg = {"title": "title"}

        desc = PlotDescription(name, *cfg)

        mw.on_make_new_subwin(name, desc)

        mw.make_subwin.assert_called_once_with(MDISubPlot, name, desc)


    def test_on_dclick_list_item(self):

        mw = self.mw
        mw.plot_single_item = mock.MagicMock()
        item = mw.lst.lst.get("sine")

        mw.on_dclick_list_item(item)

        mw.plot_single_item.assert_called_once_with(item)


    def test_on_plot_selected(self):
        mw = self.mw
        sine_item = mw.lst.lst.get("sine")
        cosine_item = mw.lst.lst.get("cosine")
        mw.lst.setCurrentItem(sine_item)
        assert mw.lst.selectedItems()
        mw.plot_single_item = mock.MagicMock()

        mw.on_plot_selected()
        mw.plot_single_item.assert_called_once_with(sine_item)

        mw.lst.setSelectionMode(QListWidget.MultiSelection)
        sine_item.setSelected(True)
        cosine_item.setSelected(True)

        assert len(mw.lst.selectedItems()) == 2

        mw.plot_multiple_items = mock.MagicMock()
        mw.on_plot_selected()
        mw.plot_multiple_items.assert_called_once_with([sine_item, cosine_item])


    def test_plot_single_item(self):
        mw = self.mw
        sine_item = mw.lst.lst.get("sine")
        sine_item.set_alarm = mock.MagicMock()
        mw.activate_or_make_subwin = mock.MagicMock()

        mw.plot_single_item(sine_item)

        sine_item.set_alarm.assert_called_once_with(False)
        mw.activate_or_make_subwin.assert_called_once_with(
            MDISubPlot, sine_item.key, sine_item.value
        )


    def test_plot_multiple_items(self):
        mw = self.mw
        sine_item = mw.lst.lst.get("sine")
        cosine_item = mw.lst.lst.get("cosine")
        names = ["sine", "cosine"]
        name = " | ".join(names)
        items = [sine_item, cosine_item]
        descs = {"sine": sine_item.value, "cosine": cosine_item.value}

        mw.activate_or_make_subwin = mock.MagicMock()

        mw.plot_multiple_items(items)

        mw.activate_or_make_subwin.assert_called_once_with(MDISubMultiPlot, name, descs)


    def test_activate_or_make_subwin(self):
        mw = self.mw
        sine_item = mw.lst.lst.get("sine")
        sub = MDISubPlot("sine", sine_item.value)
        mw.mdi.add(sub)

        cosine_item = mw.lst.get("cosine")

        mw.mdi.setActiveSubWindow = mock.MagicMock()
        assert mw.mdi.findSubWindow("sine")

        mw.activate_or_make_subwin(MDISubPlot, sine_item.key, sine_item.value)

        mw.mdi.setActiveSubWindow.assert_called_once_with(sub)

        mw.make_subwin = mock.MagicMock()
        mw.activate_or_make_subwin(MDISubPlot, cosine_item.key, cosine_item.value)
        mw.make_subwin.assert_called_once_with(
            MDISubPlot, cosine_item.key, cosine_item.value
        )


    def test_make_subwin(self):

        mw = self.mw
        cosine_item = mw.lst.get("cosine")

        mw.mdi.add = mock.MagicMock()
        # sub = MDISubPlot(cosine_item.key, cosine_item.value)

        mw.make_subwin(MDISubPlot, cosine_item.key, cosine_item.value)

        assert isinstance(mw.mdi.add.call_args[0][0], MDISubPlot)
        # mw.mdi.add.assert_called_once_with(sub)



