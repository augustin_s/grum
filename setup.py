from setuptools import setup

if __name__ == "__main__":
    setup(
        install_requires=["pyqt5", "pyqtgraph", "h5py", "PyQtWebEngine"],
        entry_points={"console_scripts": ["grum=grum:main"]},
    )


